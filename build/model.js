"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");

var VehicleSchema = new mongoose.Schema({

    placa :{type: String, required: true, unique: true},
    marca :{type: String, required: true, unique: true},
    modelo:{type: String, required: true, unique: true},
    cor   :{type: String, required: true, unique: true},

    ano_fabricacao:{type:},

    revisoes:{[

    		data_revisao:{type:},
    		valor       :{type:}

    	]},

    
    createdAt: {type: Date, default: Date.now}

});

exports.default = mongoose.model('Vehicle', CrushSchema);
