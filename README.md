# NodeJS - Creating Api for testing purposes for Maqplan

# Requisitos
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [MongoDB](https://www.mongodb.com/)
- [Postman](https://www.getpostman.com/)

# Installation

Run the following commands on a terminal:

```
git clone  https://gitlab.com/public-projects-aus/test-maqplan-api-rest.git
```

```
cd test-maqplan-api-rest
```

```
npm i
```

# To run the project

Use the command: 

```
npm start
```

# Browser or Postman

Open the following route that is specified in server.ts:

```
localhost:3000/
```

