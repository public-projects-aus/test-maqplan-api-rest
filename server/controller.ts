"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("./model");
var Controller = /** @class */ (function () {

    function Controller() {

    }

    //Select
    Controller.prototype.getVehicle = function () {
        return model_1.default.find({});
    };

    Controller.prototype.select = function (req, res) {
        this.getVehicle()
            .then(function (vehicle) { return res.status(200).json({ 'result': vehicle }); })
            .catch(function (err) { return res.status(400).json({ 'result': err }); });
    };

    //SelectOne
    Controller.prototype.getVehicleByID = function (id) {
        return model_1.default.find(id);
    };

    Controller.prototype.selectOne = function (req, res) {
        var id = { _id: req.params.id };
        this.getVehicleByID(id)
            .then(function (vehicle) { return res.status(200).json({ 'result': vehicle }); })
            .catch(function (err) { return res.status(400).json({ 'result': err }); });
    };

    //Delete Data
    Controller.prototype.deleteByID = function (id) {
        return model_1.default.deleteOne(id);
    };

    Controller.prototype.delete = function (req, res) {
        var id = { _id: req.params.id };
        this.deleteByID(id)
            .then(function (vehicle) { return res.status(200).json({ 'result': vehicle }); })
            .catch(function (err) { return res.status(400).json({ 'result': err }); });
    };

    //Update Data
    Controller.prototype.updateVehicle = function (id, data) {
        return model_1.default.findOneAndUpdate(id, data);
    };

    Controller.prototype.update = function (req, res) {
        var id      = { _id: req.params.id };
        var vehicle = req.body;
        this.updateVehicle(id, vehicle)
            .then(function (vehicle) { return res.status(200).json({ 'result': vehicle }); })
            .catch(function (err) { return res.status(400).json({ 'result': err }); });
    };

    //Insert Data
    Controller.prototype.createVehicle = function (data) {
        return model_1.default.create(data);
    };

    Controller.prototype.insert = function (req, res) {
        var vehicle = req.body;
        this.createVehicle(vehicle)
            .then(function (vehicle) { return res.status(200).json({ 'result': vehicle }); })
            .catch(function (err) { return res.status(400).json({ 'result': err }); });
    };

    return Controller;

}());

exports.default = Controller;
